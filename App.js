/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  Button,
  Alert,
  View
} from 'react-native';
import Storage from 'react-native-storage';
import { AsyncStorage } from 'react-native';
import BackgroundGeolocation from "react-native-background-geolocation";

var storage = new Storage({
	// maximum capacity, default 1000 
	size: 1000,

	// Use AsyncStorage for RN, or window.localStorage for web.
	// If not set, data would be lost after reload.
	storageBackend: AsyncStorage,
	
	// expire time, default 1 day(1000 * 3600 * 24 milliseconds).
	// can be null, which means never expire.
	defaultExpires: 1000 * 3600 * 24,
	
	// cache data in the memory. default is true.
	enableCache: true,
	
	// if data was not found in storage or expired,
	// the corresponding sync method will be invoked and return 
	// the latest data.
	sync : {
		// we'll talk about the details later.
	}
})

onClickSetHandler = () => {
  console.log('set')
  let date = new Date().getTime();
  storage.save({
    key: 'date',   // Note: Do not use underscore("_") in key!
    data: { 
      date: date
    },
    
    // if not specified, the defaultExpires will be applied instead.
    // if set to null, then it will never expire.
    expires: 1000 * 3600
  }).then(res=>console.log(res)).catch(err=>console.log(err));
}

onClickLoadHandler = () => {
  console.log('load')
  storage.load({
    key: 'date',
    
    // autoSync(default true) means if data not found or expired,
    // then invoke the corresponding sync method
    autoSync: true,
    
    // syncInBackground(default true) means if data expired,
    // return the outdated data first while invoke the sync method.
    // It can be set to false to always return data provided by sync method when expired.(Of course it's slower)
    syncInBackground: true,
    
    // you can pass extra params to sync method
    // see sync example below for example
    syncParams: {
      extraFetchOptions: {
        // blahblah
      },
      someFlag: true,
    },
  }).then(ret => {
    // found data go to then()
    Alert.alert(String(ret.date))
    console.log(ret);
  }).catch(err => {
    // any exception including data not found 
    // goes to catch()
    console.warn(err.message);
    switch (err.name) {
        case 'NotFoundError':
            // TODO;
            break;
          case 'ExpiredError':
              // TODO
              break;
    }
  })
}

export default class App extends Component<{}> {

  componentWillMount() {
    ////
    // 1.  Wire up event-listeners
    //
 
    // This handler fires whenever bgGeo receives a location update.
    BackgroundGeolocation.on('location', this.onLocation, this.onError);
 
    // This handler fires when movement states changes (stationary->moving; moving->stationary)
    BackgroundGeolocation.on('motionchange', this.onMotionChange);
 
    // This event fires when a change in motion activity is detected
    BackgroundGeolocation.on('activitychange', this.onActivityChange);
 
    // This event fires when the user toggles location-services authorization
    BackgroundGeolocation.on('providerchange', this.onProviderChange);
 
    ////
    // 2.  #configure the plugin (just once for life-time of app)
    //
    BackgroundGeolocation.configure({
      // Geolocation Config
      desiredAccuracy: 0,
      distanceFilter: 10,
      // Activity Recognition
      stopTimeout: 1,
      // Application config
      debug: true, // <-- enable this hear sounds for background-geolocation life-cycle.
      logLevel: BackgroundGeolocation.LOG_LEVEL_VERBOSE,
      stopOnTerminate: false,   // <-- Allow the background-service to continue tracking when user closes the app.
      startOnBoot: true,        // <-- Auto start tracking when device is powered-up.
      // HTTP / SQLite config
      url: 'http://yourserver.com/locations',
      batchSync: false,       // <-- [Default: false] Set true to sync locations to server in a single HTTP request.
      autoSync: true,         // <-- [Default: true] Set true to sync each location to server as it arrives.
      headers: {              // <-- Optional HTTP headers
        "X-FOO": "bar"
      },
      params: {               // <-- Optional HTTP params
        "auth_token": "maybe_your_server_authenticates_via_token_YES?"
      }
    }, (state) => {
      console.log("- BackgroundGeolocation is configured and ready: ", state.enabled);
 
      if (!state.enabled) {
        ////
        // 3. Start tracking!
        //
        BackgroundGeolocation.start(function() {
          console.log("- Start success");
        });
      }
    });
  }
 
  // You must remove listeners when your component unmounts
  componentWillUnmount() {
    // Remove BackgroundGeolocation listeners
    BackgroundGeolocation.un('location', this.onLocation);
    BackgroundGeolocation.un('motionchange', this.onMotionChange);
    BackgroundGeolocation.un('activitychange', this.onActivityChange);
    BackgroundGeolocation.un('providerchange', this.onProviderChange);
 
    // Or just remove them all-at-once
    BackgroundGeolocation.removeListeners();
  }
  onLocation(location) {
    console.log('- [event] location: ', location);
  }
  onError(error) {
    console.warn('- [event] location error ', error);
  }
  onActivityChange(activity) {
    console.log('- [event] activitychange: ', activity);  // eg: 'on_foot', 'still', 'in_vehicle'
  }
  onProviderChange(provider) {
    console.log('- [event] providerchange: ', provider);    
  }
  onMotionChange(location) {
    console.log('- [event] motionchange: ', location.isMoving, location);
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Button title='set' onPress = {onClickSetHandler}/>
        </View>
        <View style={styles.Button}>
          <Button title='load' onPress = {onClickLoadHandler}  />
        </View> 
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  Button: {
    marginTop: 20
  }
});
